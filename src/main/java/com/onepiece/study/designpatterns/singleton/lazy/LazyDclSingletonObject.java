package com.onepiece.study.designpatterns.singleton.lazy;

/** 
 * <p>Title: LazyDclSingletonObject</p>
 * <p>
 *     Description:
 *     双检锁/双重校验锁（DCL即double-checked locking）
 *     JDK版本：JDK1.5起
 *     Lazy初始化：是
 *     线程安全：是
 *     实现难度：较复杂
 *     描述：采用双锁机制，安全且多线程情况下能保持高性能
 * </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: SI-TECH </p>
 * @author huojla
 * @version 1.0
 * @createtime 2018/8/3 下午2:29
 *
 */
public class LazyDclSingletonObject {
    private static volatile LazyDclSingletonObject lazyDclSingletonObject;
    
    private LazyDclSingletonObject() {
        
    }
    
    public static LazyDclSingletonObject getInstance() {
        if (lazyDclSingletonObject == null) {
            synchronized (LazyDclSingletonObject.class) {
                if (lazyDclSingletonObject == null) {
                    return new LazyDclSingletonObject();
                }
            }
        }
        return lazyDclSingletonObject;
    }
}
