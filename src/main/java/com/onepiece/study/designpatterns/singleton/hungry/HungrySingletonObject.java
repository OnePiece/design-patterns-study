package com.onepiece.study.designpatterns.singleton.hungry;

/** 
 * <p>Title: HungrySingletonObject</p>
 * <p>
 *     Description:
 *     饿汉式
 *      Lazy初始化：否
 *      线程安全：是
 *      实现难度：易
 *      描述：这种方式比较常用，但容易产生垃圾对象
 *      优点：没有加锁，执行效率会提高
 *      缺点：类加载时就初始化，浪费内存。
 * </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: SI-TECH </p>
 * @author huojla
 * @version 1.0
 * @createtime 2018/8/3 下午2:05
 *
 */
public class HungrySingletonObject {
    private static HungrySingletonObject INSTANCE  = new HungrySingletonObject();

    private HungrySingletonObject() {

    }

    public static HungrySingletonObject getInstance() {
        return INSTANCE;
    }

    public void printMessage() {
        System.out.println("Singleton Hello World！");
    }

}
