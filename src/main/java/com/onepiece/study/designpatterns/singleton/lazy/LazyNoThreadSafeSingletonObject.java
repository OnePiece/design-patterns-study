package com.onepiece.study.designpatterns.singleton.lazy;

/** 
 * <p>Title: LazyNoThreadSafeSingletonObject</p>
 * <p>
 *     Description:
 *     懒汉式，现场不安全
 *     Lazy加载：是
 *     线程安全：否
 *     实现难度：易
 *     描述：这是最基本的实现方式，这种实现最大的问题时没有加锁synchronized，所以严格意义上并不算单例模式。
 * </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: SI-TECH </p>
 * @author huojla
 * @version 1.0
 * @createtime 2018/8/3 下午2:15
 *
 */
public class LazyNoThreadSafeSingletonObject {
    private static LazyNoThreadSafeSingletonObject lazyNoThreadSafeSingletonObject;
    private LazyNoThreadSafeSingletonObject() {

    }

    public static LazyNoThreadSafeSingletonObject getInstance() {
        if (lazyNoThreadSafeSingletonObject == null) {
            return new LazyNoThreadSafeSingletonObject();
        }
        return lazyNoThreadSafeSingletonObject;
    }
}
