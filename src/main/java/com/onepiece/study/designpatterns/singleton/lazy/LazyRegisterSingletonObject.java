package com.onepiece.study.designpatterns.singleton.lazy;

/**
 * <p>Title: LazyRegisterSingletonObject</p>
 * <p>
 *     Description:
 *     登记式/静态内部类
 *     Lazy初始化：是
 *     线程安全：是
 *     实现难度：一般
 *     描述：这种方式能够达到双检锁方式一样的功效，但是实现更简单。对静态域使用延迟初始化，应使用这种方式而不是双检锁方式。
 *     注：其中实现原理及思路还有待测试
 * </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: SI-TECH </p>
 * @author huojla
 * @version 1.0
 * @createtime 2018/8/3 下午2:41
 *
 */
public class LazyRegisterSingletonObject {
    private static class SingletonHolder {
        private static final LazyRegisterSingletonObject INSTANCE = new LazyRegisterSingletonObject();
    }

    private LazyRegisterSingletonObject() {

    }

    public static final LazyRegisterSingletonObject getInstance() {
        return SingletonHolder.INSTANCE;
    }
}
