package com.onepiece.study.designpatterns.singleton.lazy;
/** 
 * <p>Title: LazyThreadSafeSingletonObject</p>
 * <p>
 *     Description:
 *     懒汉式，线程安全
 *     lazy加载：是
 *     线程安全：是
 *     实现难度：易
 *     描述：这种方式具备很好的懒加载，能够在多线程中很好的工作，但是效率很低，99%情况下不需要同步。
 *     优点：第一次调用才初始化，避免内存浪费
 *     缺点：必须加锁synchronized才能保证单例，但加锁会影响效率。
 * </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: SI-TECH </p>
 * @author huojla
 * @version 1.0
 * @createtime 2018/8/3 下午2:18
 *
 */
public class LazyThreadSafeSingletonObject {
    private static LazyThreadSafeSingletonObject lazyThreadSafeSingletonObject;

    private LazyThreadSafeSingletonObject() {

    }

    public synchronized LazyThreadSafeSingletonObject getInstance() {
        if (lazyThreadSafeSingletonObject == null) {
            return new LazyThreadSafeSingletonObject();
        }
        return lazyThreadSafeSingletonObject;
    }
}
