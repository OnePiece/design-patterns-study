package com.onepiece.study.designpatterns.singleton.enums;
/** 
 * <p>Title: EnumsSingletonObject</p>
 * <p>
 *     Description:
 *     枚举
 *     Lazy：否
 *     线程安全：是
 *     实现难度：易
 *     描述：这种方式还没有广泛采用，但这是实现单例模式最佳方式。它更简洁，自动支持序列化机制，绝对放置多次实例化。
 *     这种方式是 Effective Java 作者 Josh Bloch 提倡的方式，它不仅能避免多线程同步问题，而且还自动支持序列化机制，防止反序列化重新创建新的对象，绝对防止多次实例化。不过，由于 JDK1.5 之后才加入 enum 特性，用这种方式写不免让人感觉生疏，在实际工作中，也很少用。
 *  不能通过 reflection attack 来调用私有构造方法。
 * </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: SI-TECH </p>
 * @author huojla
 * @version 1.0
 * @createtime 2018/8/3 下午2:49
 *
 */
public enum  EnumsSingletonObject {
    INSTANCE;
    public void whateverMethod() {
        System.out.println("Enum Singleton Init!");
    }
}
