package com.onepiece.study.designpatterns.template;

public class HummerH1Model extends HummerModel {
    @Override
    public void start() {
        System.out.println("悍马H1启动~");
    }

    @Override
    public void stop() {
        System.out.println("悍马H1停止");
    }

    @Override
    public void alarm() {
        System.out.println("悍马H1发出滴滴声音~");
    }

    @Override
    public void engineBoom() {
        System.out.println("悍马H1引擎启动");
    }
}
