package com.onepiece.study.designpatterns.template;

public abstract class HummerModel {
    private boolean isAlarm = true;
    /**
     * 
     * @Title: start
     * @Description: 首先，需要能够发动，不管是手摇还是电力发东，具体实现在实现类里面
     * @param []
     * @return void
     * @author huojla
     * @version 1.0
     * @createtime 2018/8/9 上午8:56
     */
    public abstract void start();

    /**
     *
     * @Title: stop
     * @Description: 能够发动，必须能够停止
     * @param []
     * @return void
     * @author huojla
     * @version 1.0
     * @createtime 2018/8/9 上午9:15
     */
    public abstract void stop();

    /**
     * 
     * @Title: alarm
     * @Description: 喇叭会出声音，是滴滴叫，还是哗哗叫
     * @param []
     * @return void
     * @author huojla
     * @version 1.0
     * @createtime 2018/8/9 上午9:17
     */
    public abstract void alarm();
    /**
     *
     * @Title: engineBoom
     * @Description: 引擎轰隆隆
     * @param []
     * @return void
     * @author huojla
     * @version 1.0
     * @createtime 2018/8/9 上午9:19
     */
    public abstract void engineBoom();
    /**
     * 
     * @Title: run
     * @Description: 模型应该会跑，不管是人推还是电力驱动
     * @param []
     * @return void
     * @author huojla
     * @version 1.0
     * @createtime 2018/8/9 上午9:19
     */
    public void run() {
        this.start();

        this.engineBoom();

        if (isAlarm) {
            this.alarm();
        }

        this.stop();
    }

    public void setAlarm(boolean alarm) {
        isAlarm = alarm;
    }
}
