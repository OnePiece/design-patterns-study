package com.onepiece.study.designpatterns.template;

public class HummerH2Model extends HummerModel {
    @Override
    public void start() {
        System.out.println("悍马H2启动~");
    }

    @Override
    public void stop() {
        System.out.println("悍马H2停止");
    }

    @Override
    public void alarm() {
        System.out.println("悍马H2发出滴滴声音~");
    }

    @Override
    public void engineBoom() {
        System.out.println("悍马H2引擎启动");
    }
}
