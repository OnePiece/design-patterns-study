package com.onepiece.study.designpatterns.abstractfactory.human;

public class FemaleYellowHuman extends AbstractYellowHuman {
    @Override
    public void getSex() {
        System.out.println("我是女性黄色人种！");
    }
}
