package com.onepiece.study.designpatterns.abstractfactory.color;

public abstract class AbstractFactory {
    public abstract Shape createShape(String shapeType);

    public abstract Color createColor(String colorType);
}
