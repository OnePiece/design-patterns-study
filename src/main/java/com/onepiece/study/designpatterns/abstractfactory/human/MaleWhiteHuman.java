package com.onepiece.study.designpatterns.abstractfactory.human;

public class MaleWhiteHuman extends AbstractWhiteHuman {
    @Override
    public void getSex() {
        System.out.println("我是男性白色人种！");
    }
}
