package com.onepiece.study.designpatterns.abstractfactory.color;

public class Square implements Shape {
    @Override
    public void draw() {
        System.out.println("Inside Square::draw() method.");
    }
}
