package com.onepiece.study.designpatterns.abstractfactory.human;

public interface HumanFactory {
    Human createBlackHuman();

    Human createWhiteHuman();

    Human createYellowHuman();
}
