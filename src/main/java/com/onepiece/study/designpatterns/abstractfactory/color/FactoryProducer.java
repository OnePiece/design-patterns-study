package com.onepiece.study.designpatterns.abstractfactory.color;

public class FactoryProducer {
    public AbstractFactory createFactory(String choice) {
        if (choice.equalsIgnoreCase("SHAPE")) {
            return new ShapeFactory();
        } else if (choice.equalsIgnoreCase("COLOR")) {
            return new ColorFactory();
        }
        return null;
    }
}
