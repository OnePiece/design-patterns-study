package com.onepiece.study.designpatterns.abstractfactory.color;

public class ColorFactory extends AbstractFactory{
    @Override
    public Shape createShape(String shapeType) {
        return null;
    }

    @Override
    public Color createColor(String colorType) {
        if (colorType == null) {
            return null;
        }
        if (colorType.equalsIgnoreCase("RED")) {
            return new Red();
        } else if (colorType.equalsIgnoreCase("GREEN")) {
            return new Green();
        } else if (colorType.equalsIgnoreCase("BLACK")) {
            return new Black();
        }
        return null;
    }
}
