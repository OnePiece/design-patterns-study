package com.onepiece.study.designpatterns.abstractfactory.human;

public class MaleBlackHuman extends AbstractBlackHuman {
    @Override
    public void getSex() {
        System.out.println("我是男性黑色人种");
    }
}
