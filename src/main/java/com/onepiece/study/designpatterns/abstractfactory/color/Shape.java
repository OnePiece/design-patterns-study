package com.onepiece.study.designpatterns.abstractfactory.color;

public interface Shape {
    void draw();
}
