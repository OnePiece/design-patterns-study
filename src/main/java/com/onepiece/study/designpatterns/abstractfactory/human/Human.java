package com.onepiece.study.designpatterns.abstractfactory.human;

public interface Human {
    void getColor();

    void talk();

    void getSex();
}
