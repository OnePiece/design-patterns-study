package com.onepiece.study.designpatterns.abstractfactory.human;

public class FemaleWhiteHuman extends AbstractWhiteHuman {
    @Override
    public void getSex() {
        System.out.println("我是女性白色人种！");
    }
}
