package com.onepiece.study.designpatterns.abstractfactory.color;

public interface Color {
    void fill();
}
