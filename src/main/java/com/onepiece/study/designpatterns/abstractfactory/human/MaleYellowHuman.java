package com.onepiece.study.designpatterns.abstractfactory.human;

public class MaleYellowHuman extends AbstractYellowHuman {
    @Override
    public void getSex() {
        System.out.println("我是男性黄色人种！");
    }
}
