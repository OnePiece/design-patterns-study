package com.onepiece.study.designpatterns.abstractfactory.color;

public class ShapeFactory extends AbstractFactory{

    @Override
    public Shape createShape(String shapeType) {
        if (shapeType == null ) {
            return null;
        }

        if (shapeType.equalsIgnoreCase("CIRCLE")) {
            return new Circle();
        } else if (shapeType.equalsIgnoreCase("SQUARE")) {
            return new Square();
        } else if (shapeType.equalsIgnoreCase("RECTANGLE")) {
            return new Rectangle();
        }
        return null;
    }

    @Override
    public Color createColor(String colorType) {
        return null;
    }
}
