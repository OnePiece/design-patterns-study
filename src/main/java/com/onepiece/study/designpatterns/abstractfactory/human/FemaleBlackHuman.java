package com.onepiece.study.designpatterns.abstractfactory.human;

public class FemaleBlackHuman extends AbstractBlackHuman {
    @Override
    public void getSex() {
        System.out.println("我是女性黑色人种");
    }
}
