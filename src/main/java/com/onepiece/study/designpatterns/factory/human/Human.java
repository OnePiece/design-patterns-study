package com.onepiece.study.designpatterns.factory.human;
/** 
 * <p>Title: Human</p>
 * <p>Description: 人类父接口 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: SI-TECH </p>
 * @author huojla
 * @version 1.0
 * @createtime 2018/8/3 下午3:27
 *
 */
public interface Human {
    void getColor();

    void talk();
}
