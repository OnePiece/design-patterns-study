package com.onepiece.study.designpatterns.factory.shape;

public interface Shape {
    void draw();
}
