package com.onepiece.study.designpatterns.builder;

import java.util.List;

public class BMWBuilder implements CarBuilder {
    private BMWModel bmwModel = new BMWModel();
    @Override
    public CarModel getCarModel() {
        return bmwModel;
    }

    @Override
    public void setSequence(List<ModelOperEnum> sequence) {
        bmwModel.setSequence(sequence);
    }
}
