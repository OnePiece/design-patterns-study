package com.onepiece.study.designpatterns.builder;

import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
public abstract class CarModel {
    private List<ModelOperEnum> sequence = new ArrayList<>();

    protected abstract void start();

    protected abstract void stop();

    protected abstract void alarm();

    protected abstract void engineBoom();

    public void run() {
        sequence.stream().forEach(modelOperEnum -> {
            if (modelOperEnum == ModelOperEnum.START) {
                this.start();
            } else if (modelOperEnum == ModelOperEnum.STOP) {
                this.stop();
            } else if (modelOperEnum == ModelOperEnum.ALARM) {
                this.alarm();
            } else if (modelOperEnum == ModelOperEnum.ENGINE_BOOM) {
                this.engineBoom();
            }
        });
    }
}
