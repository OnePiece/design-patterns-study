package com.onepiece.study.designpatterns.builder;

import java.util.List;

public interface CarBuilder {
    CarModel getCarModel();

    void setSequence(List<ModelOperEnum> sequence);
}
