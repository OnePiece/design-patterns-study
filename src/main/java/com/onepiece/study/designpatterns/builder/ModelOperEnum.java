package com.onepiece.study.designpatterns.builder;

/** 
 * <p>Title: ModelOperEnum</p>
 * <p>Description: 模型动作枚举类 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: SI-TECH </p>
 * @author huojla
 * @version 1.0
 * @createtime 2018/8/14 上午9:20
 *
 */
public enum ModelOperEnum {
    START,STOP,ALARM,ENGINE_BOOM;
}
