package com.onepiece.study.designpatterns.builder;

import java.util.List;

public class BenzBuilder implements CarBuilder {
    private BenzModel benzModel = new BenzModel();
    @Override
    public CarModel getCarModel() {
        return benzModel;
    }

    @Override
    public void setSequence(List<ModelOperEnum> sequence) {
        benzModel.setSequence(sequence);
    }
}
