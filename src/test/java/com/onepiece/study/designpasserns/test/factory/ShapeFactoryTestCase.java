package com.onepiece.study.designpasserns.test.factory;

import com.onepiece.study.designpatterns.factory.shape.Shape;
import com.onepiece.study.designpatterns.factory.shape.ShapeFactory;
import org.junit.Test;

public class ShapeFactoryTestCase {

    @Test
    public void testSimpleFactory() {
        ShapeFactory shapeFactory = new ShapeFactory();
        Shape circle = shapeFactory.createShape("circle");
        circle.draw();
        Shape rectangle = shapeFactory.createShape("rectangle");
        rectangle.draw();;
        Shape square = shapeFactory.createShape("square");
        square.draw();;
    }
}
