package com.onepiece.study.designpasserns.test.singleton;

import com.onepiece.study.designpatterns.singleton.hungry.HungrySingletonObject;
import org.junit.Test;

public class SingletonObjectTestCase {

    @Test
    public void testHungrySingletonObject() {
        HungrySingletonObject singletonObject = HungrySingletonObject.getInstance();
        singletonObject.printMessage();
    }

}
