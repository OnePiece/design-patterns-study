package com.onepiece.study.designpasserns.test.abstractfactory;

import com.onepiece.study.designpatterns.abstractfactory.color.AbstractFactory;
import com.onepiece.study.designpatterns.abstractfactory.color.Color;
import com.onepiece.study.designpatterns.abstractfactory.color.FactoryProducer;
import com.onepiece.study.designpatterns.abstractfactory.color.Shape;
import org.junit.Test;

public class AbstractFactoryTestCase {
    @Test
    public void testAbstractFactory() {
        AbstractFactory shapeFactory = new FactoryProducer().createFactory("SHAPE");
        Shape circle = shapeFactory.createShape("circle");
        circle.draw();

        Shape square = shapeFactory.createShape("square");
        square.draw();

        Shape rectangle = shapeFactory.createShape("rectangle");
        rectangle.draw();


        AbstractFactory colorFactory = new FactoryProducer().createFactory("COLOR");

        Color red = colorFactory.createColor("red");
        red.fill();


        Color green = colorFactory.createColor("green");
        green.fill();

        Color black = colorFactory.createColor("black");
        black.fill();
    }
}
