package com.onepiece.study.designpasserns.test.abstractfactory;

import com.onepiece.study.designpatterns.abstractfactory.human.FemaleHumanFactory;
import com.onepiece.study.designpatterns.abstractfactory.human.Human;
import com.onepiece.study.designpatterns.abstractfactory.human.HumanFactory;
import com.onepiece.study.designpatterns.abstractfactory.human.MaleHumanFactory;
import org.junit.Test;

public class HumanAbstractFactoryTestCase {

    @Test
    public void testFactory() {
        HumanFactory femaleHumanFactory = new FemaleHumanFactory();
        HumanFactory maleHumanFactory = new MaleHumanFactory();

        Human femaleBlackHuman = femaleHumanFactory.createBlackHuman();
        Human femaleWhiteHuman = femaleHumanFactory.createWhiteHuman();
        Human femaleYellowHuman = femaleHumanFactory.createYellowHuman();

        Human maleBlackHuman = maleHumanFactory.createBlackHuman();
        Human maleWhiteHuman = maleHumanFactory.createWhiteHuman();
        Human maleYellowHuman = maleHumanFactory.createYellowHuman();

        femaleBlackHuman.getColor();
        femaleBlackHuman.getSex();

        maleBlackHuman.getColor();
        maleBlackHuman.getSex();
    }
}
