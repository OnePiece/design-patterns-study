package com.onepiece.study.designpasserns.test.singleton;


import com.onepiece.study.designpatterns.singleton.enums.EnumsSingletonObject;
import org.junit.Test;

public class EnumsSingletonObjectTestCase {
    @Test
    public void testEnums() {
        EnumsSingletonObject.INSTANCE.whateverMethod();
    }
}
