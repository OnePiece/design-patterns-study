package com.onepiece.study.designpasserns.test.builder;

import com.onepiece.study.designpatterns.builder.BenzBuilder;
import com.onepiece.study.designpatterns.builder.CarBuilder;
import com.onepiece.study.designpatterns.builder.ModelOperEnum;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class BuilderPatternTestCase {

    @Test
    public void testCarModelBuilder() {
        CarBuilder benzBuilder = new BenzBuilder();
        List<ModelOperEnum> modelOperEnums = new ArrayList<>();
        modelOperEnums.add(ModelOperEnum.START);
        modelOperEnums.add(ModelOperEnum.ENGINE_BOOM);
        modelOperEnums.add(ModelOperEnum.STOP);
        benzBuilder.setSequence(modelOperEnums);

        benzBuilder.getCarModel().run();
    }
}
