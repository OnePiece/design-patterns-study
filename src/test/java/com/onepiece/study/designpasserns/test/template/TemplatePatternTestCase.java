package com.onepiece.study.designpasserns.test.template;

import com.onepiece.study.designpatterns.template.HummerH1Model;
import com.onepiece.study.designpatterns.template.HummerH2Model;
import org.junit.Test;

public class TemplatePatternTestCase {

    @Test
    public void testHummerV1() {
        HummerH1Model hummerH1Model = new HummerH1Model();
        hummerH1Model.setAlarm(false);
        hummerH1Model.run();
        System.out.println("--------------------------------");
        HummerH2Model hummerH2Model = new HummerH2Model();
        hummerH2Model.run();
    }
}
