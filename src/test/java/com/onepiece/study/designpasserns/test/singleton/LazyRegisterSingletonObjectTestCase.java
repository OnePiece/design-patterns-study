package com.onepiece.study.designpasserns.test.singleton;

/** 
 * <p>Title: LazyRegisterSingletonObjectTestCase</p>
 * <p>Description: 测试静态内部类加载过程，测试后，发现通过静态变量，实际上没有执行SingletonHolder构造方法 </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: SI-TECH </p>
 * @author huojla
 * @version 1.0
 * @createtime 2018/8/3 下午2:46
 *
 */
public class LazyRegisterSingletonObjectTestCase {
    private static class SingletonHolder {
        public SingletonHolder() {
            System.out.println("SingletonHolder 加载实例化。");
        }
        private static final LazyRegisterSingletonObjectTestCase INSTANCE = new LazyRegisterSingletonObjectTestCase();
    }

    private LazyRegisterSingletonObjectTestCase() {

    }

    public static final LazyRegisterSingletonObjectTestCase getInstance() {
        return SingletonHolder.INSTANCE;
    }

    public static void main(String[] args) {
        new LazyRegisterSingletonObjectTestCase();

        System.out.println(getInstance());
    }
}
