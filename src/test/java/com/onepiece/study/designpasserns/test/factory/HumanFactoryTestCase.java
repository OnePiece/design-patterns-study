package com.onepiece.study.designpasserns.test.factory;

import com.onepiece.study.designpatterns.factory.human.*;
import org.junit.Test;

public class HumanFactoryTestCase {
    @Test
    public void testHumanFactory() {
        AbstractHumanFactory humanFactory = new HumanFactory();
        // 黑种人
        Human blackHuman = humanFactory.createHuman(BlackHuman.class);
        blackHuman.getColor();
        blackHuman.talk();

        // 黄种人
        Human yellowHuman = humanFactory.createHuman(YellowHuman.class);
        yellowHuman.getColor();
        yellowHuman.talk();

        // 黑种人
        Human whiteHuman = humanFactory.createHuman(WhiteHuman.class);
        whiteHuman.getColor();
        whiteHuman.talk();
    }
}
